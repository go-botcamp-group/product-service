package service

import (
	"context"

	"github.com/jmoiron/sqlx"
	pb "gitlab.com/product-service/genproto/product"
	"gitlab.com/product-service/pkg/logger"
	"gitlab.com/product-service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ProductService struct {
	Storage storage.IsStorage
	Logger  logger.Logger
}

func NewProductService(db *sqlx.DB, l logger.Logger) *ProductService {
	return &ProductService{
		Storage: storage.NewStoragePg(db),
		Logger:  l,
	}
}

func (p *ProductService) CreateProduct(ctx context.Context, req *pb.Product) (*pb.ProductInfo, error) {
	res, err := p.Storage.Product().CreateProduct(req)
	if err != nil {
		p.Logger.Error("Error while creating product", logger.Any("Create", err))
		return &pb.ProductInfo{}, status.Error(codes.Internal, "Recheck user info")
	}
	return res, nil
}

func (p *ProductService) GetAllProducts(context.Context, *pb.Empty) (*pb.GetProducts, error) {
	res, err := p.Storage.Product().GetAllProducts()
	if err != nil {
		p.Logger.Error("Error while getting all prducts", logger.Any("get", err))
		return &pb.GetProducts{}, status.Error(codes.Internal, "Couldn't get products")
	}
	return res, nil
}

func (p *ProductService) GetProductsByIds(ctx context.Context, req *pb.GetProductsByIdsRequest) (*pb.GetProducts, error) {
	res, err := p.Storage.Product().GetProductsByIds(req)

	if err != nil {
		p.Logger.Error("Error while getting products by ids", logger.Any("get", err))
		return &pb.GetProducts{}, status.Error(codes.InvalidArgument, "some or all of the products that you are asking are not exist")
	}
	return res, nil
}
