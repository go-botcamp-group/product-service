package postgres

import (
	pb "gitlab.com/product-service/genproto/product"
)

func (p *ProductRepo) CreateCategory(req *pb.Category) (*pb.Category, error) {
	err := p.Db.QueryRow(`INSERT INTO categories (name) values($1) returning id`, req.Name).Scan(&req.Id)
	return req, err
}

func (p *ProductRepo) GetCategoryById(id int64) (*pb.Category, error) {
	res := &pb.Category{}
	err := p.Db.QueryRow(`SELECT id, name FROM categories WHERE id = $1`, id).Scan(
		&res.Id, &res.Name,
	)
	return res, err
}
