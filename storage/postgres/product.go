package postgres

import (
	"github.com/jmoiron/sqlx"
	pb "gitlab.com/product-service/genproto/product"
)

type ProductRepo struct {
	Db *sqlx.DB
}

func NewProductRepo(db *sqlx.DB) *ProductRepo {
	return &ProductRepo{
		Db: db,
	}
}

func (p *ProductRepo) CreateProduct(req *pb.Product) (*pb.ProductInfo, error) {

	response := &pb.ProductInfo{}

	err := p.Db.QueryRow(`INSERT INTO products(name, model, category_id, type_id) 
	values($1, $2, $3, $4) returning id`,
		req.Name, req.Model, req.CategoryId, req.TypeId).Scan(
		&response.Id,
	)
	if err != nil {
		return &pb.ProductInfo{}, err
	}
	response.Model = req.Model
	response.Name = req.Name
	response.Category, err = p.GetCategoryById(req.CategoryId)
	if err != nil {
		return &pb.ProductInfo{}, err
	}
	response.Type, err = p.GetTypeById(req.TypeId)
	if err != nil {
		return &pb.ProductInfo{}, err
	}
	return response, nil
}

func (p *ProductRepo) GetAllProducts() (*pb.GetProducts, error) {
	rows, err := p.Db.Query(`SELECT id, name, model, category_id, type_id FROM products`)
	if err != nil {
		return &pb.GetProducts{}, err
	}
	response := &pb.GetProducts{}
	for rows.Next() {
		temp := &pb.ProductInfo{}
		var c_id, t_id int64
		err := rows.Scan(&temp.Id, &temp.Name, &temp.Model, &c_id, &t_id)
		if err != nil {
			return &pb.GetProducts{}, err
		}
		temp.Category, err = p.GetCategoryById(c_id)
		if err != nil {
			return &pb.GetProducts{}, err
		}
		temp.Type, err = p.GetTypeById(t_id)
		if err != nil {
			return &pb.GetProducts{}, err
		}
		response.Products = append(response.Products, temp)
	}
	return response, nil
}

func (p ProductRepo) GetProductsByIds(req *pb.GetProductsByIdsRequest) (*pb.GetProducts, error) {
	response := &pb.GetProducts{}
	for _, id := range req.Ids {
		temp := &pb.ProductInfo{}
		var c_id, t_id int64
		err := p.Db.QueryRow(`SELECT 
		id, 
		name, 
		model, 
		category_id, 
		type_id 
		FROM products where id = $1`, id).Scan(
			&temp.Id,
			&temp.Name, &temp.Model, &c_id, &t_id,
		)
		if err != nil {
			return response, err
		}

		temp.Category, err = p.GetCategoryById(c_id)
		if err != nil {
			return &pb.GetProducts{}, err
		}
		temp.Type, err = p.GetTypeById(t_id)
		if err != nil {
			return &pb.GetProducts{}, err
		}
		response.Products = append(response.Products, temp)
	}
	return response, nil
}
