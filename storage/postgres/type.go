package postgres

import (
	pb "gitlab.com/product-service/genproto/product"
)

func (p *ProductRepo) CreateType(req *pb.Type) (*pb.Type, error) {
	err := p.Db.QueryRow(`INSERT INTO types (name) values($1) returning id`, req.Name).Scan(&req.Id)
	return req, err
}

func (p *ProductRepo) GetTypeById(id int64) (*pb.Type, error) {
	res := &pb.Type{}
	err := p.Db.QueryRow(`SELECT id, name FROM types where id = $1`, id).Scan(
		&res.Id,
		&res.Name,
	)
	return res, err
}