CREATE TABLE IF NOT EXISTS products (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL UNIQUE,
    model TEXT,
    category_id INTEGER NOT NULL REFERENCES categories(id),
    type_id INTEGER NOT NULL REFERENCES types(id) 
);